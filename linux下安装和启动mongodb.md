#### 说明
一般linux的软件安装位置在 /usr/local 下，所以要安装什么软件就先在这个目录下面新建相应的文件夹
安装包一般放在 /usr/local/src 目录下，也可以根据个人喜好放在不同的目录
#### 安装步骤和方法
- 新建mongodb目录

```
  $ cd /usr/local
  $ mkdir mongodb
```
- 在任意位置执行以下命令下载mongodb安装包

```
$ wget http://fastdl.mongodb.org/linux/mongodb-linux-x86_64-2.0.4.tgz #这个地址根据要安装的版本决定
```
- 解压安装包

```
$ tar -zxvf mongodb-linux-x86_64-2.0.4.tgz
```
- 移动刚解压的文件夹下的所有内容到mongodb目录下
```
$ cd mongodb-linux-x86_64-2.0.4
$ mv * /usr/local/mongodb
```
- 创建data文件夹用于存储数据，官方教程是在根目录创建
```
$ mkdir /data/mongodb
```
- 创建日志文件夹和.log日志文件用于存放日志，个人把日志文件放在 /var/log/mongodb 目录下
```
$ cd /var/log
$ mkdir mongodb
$ cd mongodb
$ touch mongo.log
```
到此，mongodb的安装其实已经完成了，接下来就是启动和配置的问题了。
#### 启动mongodb
1、带参数启动
- 进入mongodb安装目录下的bin目录
```
$ cd /usr/local/mongodb/bin
```
- 执行命令
```
$ ./mongod --dbpath=/data/mongodb --logpath=/var/log/mongodb/mongo.log
```
- 后台启动（以守护进程的方式运行MongoDB）
```
$ ./mongod --dbpath=/data/mongodb --logpath=/var/log/mongodb/mongo.log --fork
```
- 后台权限启动
```
$ ./mongod --dbpath=/data/mongodb --logpath=/var/log/mongodb/mongo.log --fork --auth
```
- 打开mongodb客户端
```
$ ./mongo
```
如果能正常连接mongodb服务说明启动成功

2、配置文件启动
- 新建mongodb配置文件
```
$ cd /etc
$ touch mongod.conf
```
配置文件示例(详细配置请参考官网)
```
port=27017 
dbpath=/data/mongodb 
logpath=/var/log/mongodb/mongo.log
logappend=true 
fork=true 
bindIp=0.0.0.0
```
启动服务
```
$ ./mongod --config /etc/mongod.conf
```
如果想全局使用mongod或者mongo等命令，需要设置环境变量，不会的百度谷歌就好了。
