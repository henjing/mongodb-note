#### 开机自启动设置

- #### 方法一
如果用的是带参数的方式启动服务的话，例如：启动命令为

``
$ /usr/local/mongodb/bin/mongod -dbpath=/data/mongodb -logpath=/var/log/mongodb --fork
``

那么把这句话写到把 /etc/rc.local，即可开机启动。

- #### 方法二

将一个脚本放到/etc/init.d/ 下

比如，命名为/etc/init.d/mongodb，则执行以下步骤即可：

1、要给它们赋予执行权限：
chmod +x /etc/init.d/mongodb


2、接着试一下是否可以启动、停止：
service mongodb start
service mongodb stop


3、最后设为开机启动：
chkconfig mongodb on

这个脚本具体内容稍后更新